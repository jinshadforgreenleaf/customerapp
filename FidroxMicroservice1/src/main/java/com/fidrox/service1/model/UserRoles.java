package com.fidrox.service1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fidrox.service1.config.AppConstants;

@Entity
@Table(name = AppConstants.TABLE_NAMES.USER_ROLES)
public class UserRoles implements Serializable {

	private static final long serialVersionUID = 4918815200703309714L;

	public UserRoles() {
		super();
	}

	public UserRoles(int role_id) {
		super();
		this.role_id = role_id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id")
	private int role_id;

	@Column(name = "role_name")
	private String role_name;

	@Column(name = "role_code")
	private String role_code;

	@Column(name = "status")
	private int status;

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getRole_code() {
		return role_code;
	}

	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
