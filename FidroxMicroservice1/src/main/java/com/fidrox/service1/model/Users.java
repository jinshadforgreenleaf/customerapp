package com.fidrox.service1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fidrox.service1.config.AppConstants;

@Entity
@Table(name = AppConstants.TABLE_NAMES.USERS)
public class Users implements Serializable {

	private static final long serialVersionUID = -6850118112554767056L;

	public Users() {
		super();
	}

	public Users(String user_name, String date_of_birth, char gender, String phone_number, UserRoles userRole,
			int status) {
		super();
		this.user_name = user_name;
		this.date_of_birth = date_of_birth;
		this.gender = gender;
		this.phone_number = phone_number;
		this.userRole = userRole;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int user_id;

	@Column(name = "user_name")
	private String user_name;

	@Column(name = "date_of_birth")
	private String date_of_birth;

	@Column(name = "gender")
	private char gender;

	@Column(name = "phone_number")
	private String phone_number;

	@OneToOne
	@JoinColumn(name = "role_id")
	private UserRoles userRole;

	@Column(name = "status")
	private int status;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public UserRoles getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRoles userRole) {
		this.userRole = userRole;
	}

}
