package com.fidrox.service1.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.fidrox" })
@SpringBootApplication(exclude=HibernateJpaAutoConfiguration.class)
public class FidroxMicroservice1Application {

	public static void main(String[] args) {
		SpringApplication.run(FidroxMicroservice1Application.class, args);
	}

}
