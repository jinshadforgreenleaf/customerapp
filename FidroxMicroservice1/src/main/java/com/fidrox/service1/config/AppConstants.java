package com.fidrox.service1.config;

public class AppConstants {

	public static class TABLE_NAMES {
		public static final String USER_ROLES = "user_roles";
		public static final String USERS = "users";
	}

	public static final int STATUS_ACTIVE = 1;

}
