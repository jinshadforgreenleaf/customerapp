package com.fidrox.service1.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class HibernateConfig {

	@Autowired
	private Environment environment;

	@Bean
	public LocalSessionFactoryBean sessionFactory() throws IllegalStateException, Exception {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.fidrox.service1.model"});
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() throws IllegalStateException, Exception {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		String mode = environment.getRequiredProperty("app.mode");

		dataSource.setDriverClassName(environment.getRequiredProperty(mode + ".jdbc.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty(mode + ".jdbc.url"));
		dataSource.setUsername(environment.getRequiredProperty(mode + ".jdbc.username"));
		dataSource.setPassword((environment.getRequiredProperty(mode + ".jdbc.password")));
		return dataSource;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.connection.CharSet", environment.getProperty("hibernate.connection.charset"));
		properties.put("hibernate.connection.characterEncoding", environment.getProperty("hibernate.connection.characterencoding"));
		properties.put("hibernate.connection.useUnicode", environment.getProperty("hibernate.connection.useunicode"));

		 

		/*
		 * properties.put("hibernate.c3p0.min_size",
		 * environment.getRequiredProperty("hibernate.c3p0.min_size"));
		 * properties.put("hibernate.c3p0.max_size",
		 * environment.getRequiredProperty("hibernate.c3p0.max_size"));
		 * properties.put("hibernate.c3p0.timeout",
		 * environment.getRequiredProperty("hibernate.c3p0.timeout"));
		 * properties.put("hibernate.c3p0.max_statements",
		 * environment.getRequiredProperty("hibernate.c3p0.max_statements"));
		 */
		// properties.put("hibernate.current_session_context_class",environment.getRequiredProperty("hibernate.current_session_context_class"));

		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);
		return txManager;
	}

}
