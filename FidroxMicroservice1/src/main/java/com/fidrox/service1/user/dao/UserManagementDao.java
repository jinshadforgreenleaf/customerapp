package com.fidrox.service1.user.dao;

import java.util.Map;

import com.fidrox.service1.model.Users;

public interface UserManagementDao {

	public Map createUser(Users user) throws Exception;

	public Users getUser(int user_id) throws Exception;

}
