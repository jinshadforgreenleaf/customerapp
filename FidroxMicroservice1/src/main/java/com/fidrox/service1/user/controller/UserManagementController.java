package com.fidrox.service1.user.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fidrox.service1.config.AppConstants;
import com.fidrox.service1.model.UserRoles;
import com.fidrox.service1.model.Users;
import com.fidrox.service1.user.service.UserManagementService;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/usermanagement")
public class UserManagementController {

	@Autowired
	UserManagementService service;

////	@RequestMapping(value = "/createuser", method = RequestMethod.POST)
//	@PostMapping(value = "/createuser", consumes = "application/json", produces = "application/json")
//	@ResponseBody
//	public Map createUser(@RequestParam(value = "user_name") String user_name,
//			@RequestParam(value = "date_of_birth") String date_of_birth, @RequestParam(value = "gender") char gender,
//			@RequestParam(value = "phone_number") String phone_number, @RequestParam(value = "user_role") int user_role)
//			throws Exception {
//
//		Users user = new Users(user_name, date_of_birth, gender, phone_number, new UserRoles(user_role),
//				AppConstants.STATUS_ACTIVE);
//
//		return service.createUser(user);
//	}

	@PostMapping(value = "/createuser", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public Map createUser(@RequestBody Users user) throws Exception {

//		Users user = new Users(user_name, date_of_birth, gender, phone_number, new UserRoles(user_role),
//				AppConstants.STATUS_ACTIVE);

		user.setStatus(AppConstants.STATUS_ACTIVE);

		return service.createUser(user);
	}

	@RequestMapping(value = "/getuser", method = RequestMethod.GET)
	@ResponseBody
	public Users getUser(@RequestParam(value = "user_id") int user_id) throws Exception {
		return service.getUser(user_id);
	}

}
