package com.fidrox.service1.user.dao;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fidrox.service1.config.AbstractDao;
import com.fidrox.service1.model.Users;

@Repository("UserManagementDao")
@Transactional
public class UserManagementDaoImpl extends AbstractDao<Integer, Users> implements UserManagementDao {

	public Map createUser(Users user) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		try {

			save(user);

			data.put("user", user);
			data.put("status", true);
			data.put("message", "Created successfully.");

		} catch (Exception e) {
			data.put("status", false);
			data.put("message", "Failed when creating");
		}
		return data;
	}

	public Users getUser(int user_id) throws Exception {
		return getByKey(user_id);
	}

}
