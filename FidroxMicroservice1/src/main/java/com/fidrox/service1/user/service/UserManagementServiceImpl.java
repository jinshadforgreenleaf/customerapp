package com.fidrox.service1.user.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidrox.service1.model.Users;
import com.fidrox.service1.user.dao.UserManagementDao;

@Service("userManagementService")
public class UserManagementServiceImpl implements UserManagementService {

	@Autowired
	UserManagementDao dao;

	public Map createUser(Users user) throws Exception {
		return dao.createUser(user);
	}

	public Users getUser(int user_id) throws Exception {
		return dao.getUser(user_id);
	}

}
